<!--
SPDX-FileCopyrightText: 2023 Ben Bonacci <[myfirstname] at [myfullname] dot com>
SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL
-->

# TO-DO
If you're looking for a task to work on, feel free to try some of these items:

- Have a better exception system (Currently, if the Matrix access token expires the script doesn't say anything)

- Use chat_rooms.json to send bug reports to multiple rooms

- Be asynchronous when making HTTP requests

- Use V3 of the Matrix API

- Have better documentation (comments and guide)

- Have deployable development and production containers

- Provide monthly insights with bug data

Other improvements and fixes are also welcome!
