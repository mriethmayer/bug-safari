#!/usr/bin/env python

""""""
# Bug Safari - A simple script that queries Bugzilla for Plasma bug statistics and sends a report to the #plasma:kde.org room on Matrix."""

# SPDX-FileCopyrightText: 2023 Ben Bonacci <[myfirstname] at [myfullname] dot com>
# SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

""""""

import json
import os
import requests
from time import sleep
from random import randint

# Load the json file with the Bugzilla and Matrix secrets
with open('secrets.json') as f:
	secrets = json.load(f)
	print("Loaded secrets.json")

# Load the json file with the Bugzilla lists to report on
with open('bug_lists.json') as f:
	lists = json.load(f)
	print("Loaded bug_lists.json")

# Load the json file with the Matrix rooms to send reports to
#with open('chat_rooms.json') as f:
	#rooms = json.load(f)
	#print("Loaded chat_rooms.json")

# Prepare secrets
conf_safari_name = secrets["conf_safari_name"]
conf_safari_debug = secrets["conf_safari_debug"]
conf_safari_delta = secrets["conf_safari_delta"]
conf_matrix_homeserver = secrets["conf_matrix_homeserver"]
conf_matrix_room = secrets["conf_matrix_room"]

bugzillaToken = secrets["bugzilla_token"]
matrixToken = secrets["matrix_token"]
print("Secrets ready")

# Prepare headers
bugzillaHeader = { "User-Agent": "bug-safari (" + conf_safari_name + ") 1.0.0", "X-BUGZILLA-API-KEY": bugzillaToken}
matrixHeader = {"User-Agent": "bug-safari (" + conf_safari_name + ") 1.0.0", "Authorization": "Bearer " + matrixToken}
print("Headers ready")

# Use Bugzilla's API to check status of bug lists
print("Downloading bug lists...")

i = 1
for list in lists.values():
	try:
		if conf_safari_delta == True:
			os.rename("temp/" + str(i) + ".json", "temp/" + str(i) + "_last.json")
			pass
		with open("temp/" + str(i) + ".json", 'w') as f:
			json.dump(requests.get(list, headers=bugzillaHeader).json(), f)
		f.close()
		i = i + 1
		sleep(randint(1, 5))
	except Exception as e:
		print(e)
		exit(1)

# Use Matrix's API to send bug status report to the room
print("Sending to Matrix...")

matrixJson = {"body": "", "format": "org.matrix.custom.html", "formatted_body": "<b>Today's " + conf_safari_name + " bug report:</b>", "msgtype": "m.notice"}
requests.post(conf_matrix_homeserver + "/_matrix/client/r0/rooms/" + conf_matrix_room + "/send/m.room.message", headers=matrixHeader, json=matrixJson)
sleep(randint(1, 5))

i = 1
for listName, listUrl in lists.items():
	try:
		with open("temp/" + str(i) + ".json") as f:
			report = json.load(f)
		if conf_safari_delta == True:
			with open("temp/" + str(i) + "_last.json") as f:
				delta = json.load(f)
			matrixJson = {"body": "", "format": "org.matrix.custom.html", "formatted_body": "<a href='" + listUrl.replace("/rest/bug", "/buglist.cgi") + "'>" + str(len(report["bugs"])) + " " + listName + " (" + str(len(report["bugs"]) - len(delta["bugs"])) + " new bugs since last report)" + "</a>", "msgtype": "m.notice"}
		else:
			matrixJson = {"body": "", "format": "org.matrix.custom.html", "formatted_body": "<a href='" + listUrl.replace("/rest/bug", "/buglist.cgi") + "'>" + str(len(report["bugs"])) + " " + listName + "</a>", "msgtype": "m.notice"}
		requests.post(conf_matrix_homeserver + "/_matrix/client/r0/rooms/" + conf_matrix_room + "/send/m.room.message", headers=matrixHeader, json=matrixJson)
		if conf_safari_debug == True:
			print(matrixJson)
		i = i + 1
		sleep(randint(1, 5))
	except Exception as e:
		print(e)
		exit(1)

matrixJson = {"body": "", "format": "org.matrix.custom.html", "formatted_body": "<b>See you tomorrow for the next report!</b>", "msgtype": "m.notice"}
requests.post(conf_matrix_homeserver + "/_matrix/client/r0/rooms/" + conf_matrix_room + "/send/m.room.message", headers=matrixHeader, json=matrixJson)
sleep(randint(1, 5))

# Finish
print("Done!")
