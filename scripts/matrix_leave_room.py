#!/usr/bin/env python

""""""
# A helper script to leave room on Matrix

# SPDX-FileCopyrightText: 2023 Ben Bonacci <[myfirstname] at [myfullname] dot com>
# SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

""""""

from getpass import getpass
import requests

# Ask for Matrix credentials
matrixHomeserver = input("Matrix homeserver: ")
matrixRoom = input("Matrix room ID: ")
matrixToken = getpass("Matrix access token: ")

# Try to authenticate with the homeserver
matrixHeader = {"Authorization": "Bearer " + matrixToken}
try:
    matrixResponse = requests.post(matrixHomeserver + "/_matrix/client/api/v1/rooms/" + matrixRoom + "/leave", headers=matrixHeader)
    matrixResponse.raise_for_status()

except requests.exceptions.HTTPError as e:
    print(e)
    exit(1)

# Print the JSON response data
print(matrixResponse.json())
print("You have left the room")
