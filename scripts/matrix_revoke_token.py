#!/usr/bin/env python

""""""
# A helper script to generate a Matrix access token

# SPDX-FileCopyrightText: 2023 Ben Bonacci <[myfirstname] at [myfullname] dot com>
# SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

""""""

from getpass import getpass
import requests

# Ask for Matrix credentials
matrixHomeserver = input("Matrix homeserver: ")
matrixToken = getpass("Matrix access token: ")

# Try to authenticate with the homeserver
matrixHeader = {"Authorization": "Bearer " + matrixToken}
try:
    matrixResponse = requests.post(matrixHomeserver + "/_matrix/client/r0/logout", headers=matrixHeader)
    matrixResponse.raise_for_status()

except requests.exceptions.HTTPError as e:
    print(e)
    exit(1)

# Print the JSON response data
print(matrixResponse.json())
print("Your access token is now revoked")
