#!/usr/bin/env python

""""""
# A helper script to convert Bugzilla list URLs to the API equivalent

# SPDX-FileCopyrightText: 2023 Ben Bonacci <[myfirstname] at [myfullname] dot com>
# SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

""""""

# Ask for Bugzilla list URL
bugzillaOriginalUrl = input("Bugzilla list URL: ")

# Print the converted URL
bugzillaApiUrl = bugzillaOriginalUrl.replace("/buglist.cgi", "/rest/bug")
print(bugzillaApiUrl)
print("Your API URL is displayed above")
print("Copy the contents to bug_lists.json")
