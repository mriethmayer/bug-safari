#!/usr/bin/env python

""""""
# A helper script to change the topic in a room on Matrix

# SPDX-FileCopyrightText: 2023 Ben Bonacci <[myfirstname] at [myfullname] dot com>
# SPDX-License-Identifier: LicenseRef-KDE-Accepted-GPL

""""""

from getpass import getpass
import requests

# Ask for Matrix credentials
matrixHomeserver = input("Matrix homeserver: ")
matrixRoom = input("Matrix room ID: ")
matrixTopic = input("Matrix topic: ")
matrixToken = getpass("Matrix access token: ")

# Try to authenticate with the homeserver
matrixHeader = {"Authorization": "Bearer " + matrixToken}
matrixJson = {"topic": matrixTopic, "msgtype": "m.room.topic"}
try:
    matrixResponse = requests.put(matrixHomeserver + "/_matrix/client/r0/rooms/" + matrixRoom + "/state/m.room.topic", headers=matrixHeader, json=matrixJson)
    matrixResponse.raise_for_status()

except requests.exceptions.HTTPError as e:
    print(e)
    exit(1)

# Print the JSON response data
print(matrixResponse.json())
print("You have changed the topic")
